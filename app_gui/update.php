<?php
  require __DIR__ . '/vendor/autoload.php';
  use Jumbojett\OpenIDConnectClient;

?>

<!DOCTYPE html>
<html lang="en">
    <?php include 'header.php';?>
	<body>
            <div class="container">
                <h1 class="text-center title">Application form for OpenStack projects</h1>
                <h2 class="text-center title">Request project update</h2>
                     <?php 
                        require_once "api.php";
                        $formType='update';
                        $flavors = Api::getFile();
                        $required=$flavors['web::required_inputs_common'];
                        if (array_key_exists('message', $flavors)) { 
                            echo "<h1 style='color:red' class='text-center'>Something happend</h1>";
                            echo "<p class='text-center'>". $flavors['message'] . "</p>";
                        } else {
                            include 'typeOfProject.php';
                            echo "<div id='set-view'>
                              <button onclick='change()' id='view-button'><i id='view' class='fas fa-th'></i></button>
                              <p id='info-view'>grid/list</p>
                            </div>";

			    echo "<div class='row' id='lines'>"; 
                            ?>
                            <div id="hlavne" class="col-md-12">
                            <?php 
                            $data                 = $flavors['cloud::profile::kolla::nova::controller::os_flavors'];
                            $boxes                = $flavors['web::inputs_box'];
                            $blacklist_attributes = $flavors['web::blacklist_attributes'];
                            $blacklist_flavors    = $flavors['web::blacklist_flavors'];
                            foreach ($data as $flav) {
                               if (in_array($flav['name'], $blacklist_flavors)) {
                                   continue;
                               }
                               echo "<div class=\"item col-md-2 cube\" style='margin-top:20px; display:inline-block;'>
                                         <div class=\"pozadie\">
                                              <div class=\"hore\">
                                                   <div class=\"pocitatko\">
                                                        <button onclick=\"incrementNumber('" . $flav['name'] . "')\" class=\"plus\" type=\"button\"><b>+</b></button>
                                                        <h4 id=\"" . $flav['name'] . "-number\" class=\"ciselko-v-pocitatku\">0</h4>
                                                        <button onclick=\"decrementNumber('" . $flav['name'] . "')\" class=\"minus\" type=\"button\"><b>-</b></button>
                                                   </div>
                                                   <h3 class=\"nazov-flavoru\">" . $flav['name'] . "</h3>
                                              </div>
    
                                              <div id='" . $flav['name'] . "' class='info' style='display: block;'>
                                                  <div class='code'>";
                                                      
                                                          foreach($flav as $key => $value) {
                                                              if (in_array($key, $blacklist_attributes, true)) {
                                                                  continue;
                                                              }
                                                              echo "
                                                                 <div class='attribute'>
                                                                    <p class='name-of-attribute'>" . $key . ":</p>
                                                                    <p class='value-of-attribute'>" . $value ."</p>
                                                                 </div>";
                                                          }
                                             
                                                echo "
                                                 </div>
                                              </div>
                                         </div>
                                      </div>";
                            }
                            ?>
                            </div>
                            </div>
                            <h2 id="title-information" class="offset-top text-center"></h2>
                            <div class="row informations">
                                <?php
                                /*foreach ($boxes as $box) {
					if ($box == "floatingip"){
						echo "<div class='additional-information col-md-4'>
                                            		<h4 style='text-align:center'>
								<label >Select number of requesting floating IPs:</label>
								<select id='floatingip'>
									<option value=1>1</option>
									<option value=2>2</option>
									<option value=3>3</option>
									<option value=4>4</option>
								</select>
							</h4>
                                            	       </div>";
					}
			    }*/	
                                ?>
                            </div>
                            <div style='display:none' id="loader"></div>
                            <p id='iseverythingok' class='error'></p>
                            
                            <?php echo "<button onclick=\"sendUpdate('" . $email_of_user . "', '" . $profile_of_user . "')\" id='sum-button'><b>SEND REQUEST</b></button>";
                        }
                      ?>

	    </div>
	</body>
</html>


