# os-projects-gui

This projects represents GUI interface for requesting OpenStack project quota.
https://projects.cloud.muni.cz

## How to deploy
Instance in OpenStack deployed by terraform. 
* Copy your `key` to terraform dir add `public key` to terrafrom `terraform.tfvars`
* `source` your application credentials 
* `terraform apply`
* run `ansible-playbook manage-os-vm.yml -t deploy -l projects.cloud.muni.cz`

## How to update
Application code lacated in `app_gui` catalog. If you want to update just push updateds to that dorectory and run pupet on host.
