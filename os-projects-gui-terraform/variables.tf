variable "floating_ip" {
    type = string
    description = "Service IP"
}

variable "public_key" {
    type = string
    description = "public key"
}
