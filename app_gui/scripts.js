function incrementNumber(name) {
    document.getElementById(name + "-number").innerHTML = Number(document.getElementById(name + "-number").innerHTML) + 1;
}

function decrementNumber(name) {
    if (Number(document.getElementById(name + "-number").innerHTML) == 0) {
        return;
    } else {
        document.getElementById(name + "-number").innerHTML = Number(document.getElementById(name + "-number").innerHTML) - 1;
    }
}

// CHANGE VIEW TO GRID OR LIST
function change() {

      var item = document.getElementsByClassName("item");
      var hlavne = document.getElementById('hlavne');
      var informations = document.getElementsByClassName('nazov-flavoru');

      if (!item[0].classList.contains('cube')) {
          for (i = 0; i < item.length; i++) {
              item[i].classList.add("col-md-2");
              item[i].classList.add("cube");
          }
          hlavne.classList.remove("col-md-7");
          hlavne.classList.remove("offset-md-2");
          hlavne.classList.add("col-md-12");

      } else {
          for (i = 0; i < item.length; i++) {
              item[i].classList.remove("col-md-2");
              item[i].classList.remove("cube");
          }
          hlavne.classList.remove("col-md-12");
          hlavne.classList.add("col-md-7");
          hlavne.classList.add("offset-md-2");
      }

      var view = document.getElementById("view");
      if (view.classList.contains("fa-th")) {
          view.classList.remove("fa-th");
          view.classList.add("fa-list");
      } else {
          view.classList.remove("fa-list");
          view.classList.add("fa-th");
      }
}

function checkdate() {
  var input = document.getElementById("expiration");
  var showError = document.getElementById('iseverythingok');
  var validformat = /^\d{2}\.\d{2}\.\d{4}$/ //Basic check for format validity
  var errors = [];
  var requiredElements = document.querySelectorAll('.required');

  if(requiredElements.length > 1 || input.value !=='' ){
    if (!validformat.test(input.value)){
        errors.push("Invalid Date Format. Please correct and submit again.");
        showError.innerHTML = errors.join();
        return "error";
    
      } else { //Detailed check for valid date ranges
        var dayfield = input.value.split(".")[0]
        var monthfield = input.value.split(".")[1]
        var yearfield = input.value.split(".")[2]
        var dayobj = new Date(yearfield, monthfield - 1, dayfield)
        var currdate = new Date()
        if (currdate > dayobj){
            errors.push("Invalid Date Period. Please correct and submit again.");
            showError.innerHTML = errors.join();
          return "error";
        }
        if ((dayobj.getMonth() + 1 != monthfield) || (dayobj.getDate() != dayfield) || (dayobj.getFullYear() != yearfield)){
            errors.push("Invalid Day, Month, or Year range detected. Please correct and submit again.");
            showError.innerHTML = errors.join();
          return "error";
        } else
          return "ok"
      }
  }
}

function toggleProjectOrganization() {
    var projectOrganization = $('#project-organization');
    var projectOrganizationCustom = $('#project-organization-custom-box');
    if(projectOrganization.val() == "custom"){
        projectOrganizationCustom.show();
    } else {
        projectOrganizationCustom.hide();
    }
}

function fetchUserData(){
    return new Promise((resolve) => {
        var url = 'https://projects.brno.openstack.cloud.e-infra.cz/oauth2/userinfo'; 
        $.get(url, function(data) {
            resolve(data)
        }).fail(function(jqXHR, textStatus, errorThrown) {
        console.error('Error:', textStatus, errorThrown);
        });
      });
}

async function sendCreate(email, profile, einfraid = '') {
   
   if(!email && !profile && !einfraid){
     var result = await fetchUserData();
     email = result.email;
     profile = result.preferredUsername;
     einfraid = result.user;
   } 
   var showError                 = document.getElementById('iseverythingok');
   showError.innerHTML = '';
   var errors                    = [];
   var data                      = {};

   var nameOfTheProject          = document.getElementById('name-of-new-project');
   var description               = document.getElementById('description-of-new-project');
   var expiration                = document.getElementById('expiration');
   var additional                = document.getElementById('additional-info');
   var impact                    = document.getElementById('impact');
   var projectOrganization       = document.getElementById('project-organization');
   var projectOrganizationCustom = document.getElementById('project-organization-custom');
   var projectInvestigators      = $('#name-of-project-managers');

   var date = checkdate();

   if (nameOfTheProject.value == "" || description.value == "" || expiration.value == "" ||  projectOrganization.value == "custom" && projectOrganizationCustom.value == "" ) {
       errors.push("You must fill in a name of the project, a description of the project, expiration and project organization.");
   } else {
       data['Name_of_the_project'] = nameOfTheProject.value;
       data['Action'] = 'create';
       data['Description']  = description.value;
       data['Expiration']   = expiration.value;
       data['Additional']   = additional.value;
       data['Impact']       = impact.value;
       if( projectOrganization.value == "custom"){
        data['Organization'] = projectOrganizationCustom.value;
       } else {
        data['Organization'] = projectOrganization.value
       }
       if(validatePI(projectInvestigators.val())){
        data['projectInvestigators'] = projectInvestigators.val()
       } else {
        errors.push("Field project manager/investigator have to be in this exact format name surname mail.")
       }
   }

   var newUrl = '/send.php?';

   var additionalInformation = document.querySelector('#floatingip');
   //for (i = 0; i < additionalInformation.length; i++) {
       newUrl += `${additionalInformation.id}=${additionalInformation.value}&`;
   //}


   if (errors.length > 0 || showError.innerHTML !=='') {
       showError.innerHTML += (showError.innerHTML ? '<br>' : '') + errors.join('<br>');
       return;
   } else {
      showError.style.display = "none";
   }

   // HIDE EVERYTHING AND SHOW LOADER
   hideEverything();
   document.getElementById('loader').style.display = "block";

   var result = sumResources();
   console.log(result);

   for (var key in data) {
       var value = data[key];
       newUrl += `${key}=${value}&`;
   }
   newUrl += `email=${email}&`;
   newUrl += `profile=${profile}&`;
   newUrl += `einfra_id=${einfraid}&`
   newUrl += `types=${result[1]}&`;

   window.location.href = newUrl.concat(`result=${result[0]}`);
}

async function sendUpdate(email, profile, einfraid = '') {
    
    if(!email && !profile && !einfraid){
      var result = await fetchUserData();
      email = result.email;
      profile = result.preferredUsername;
      einfraid = result.user;
    } 
    var showError                 = document.getElementById('iseverythingok');
    showError.innerHTML = '';
    var errors                    = [];
    var data                      = {};
 
    var nameOfTheProject          = document.getElementById('name-of-new-project');
    var description               = document.getElementById('description-of-new-project');
    var expiration                = document.getElementById('expiration');
    var additional                = document.getElementById('additional-info');
    var impact                    = document.getElementById('impact');
    var projectOrganization       = document.getElementById('project-organization');
    var projectOrganizationCustom = document.getElementById('project-organization-custom');
    var projectInvestigators      = $('#name-of-project-managers');
 
    var date = checkdate();
 
    if (nameOfTheProject.value == "" ||  projectOrganization.value == "custom" && projectOrganizationCustom.value == "" ) {
        if(nameOfTheProject.value == "" ){
            errors.push("You must fill in a name of the project.");
        } else if(projectOrganization.value == "custom" && projectOrganizationCustom.value == ""){
            errors.push("Custom organisation need's to be filled.");
        }
    } else {
        data['Name_of_the_project'] = nameOfTheProject.value;
        data['Action'] = 'update';
        data['Description']  = description.value;
        data['Expiration']   = expiration.value;
        data['Additional']   = additional.value;
        data['Impact']       = impact.value;
        if( projectOrganization.value == "custom"){
         data['Organization'] = projectOrganizationCustom.value;
        } else {
         data['Organization'] = projectOrganization.value
        }
        if(projectInvestigators.val() !== ''){
            if(validatePI(projectInvestigators.val())){
                data['projectInvestigators'] = projectInvestigators.val()
            } else {
                errors.push("Field project manager/investigator have to be in this exact format name surname mail.")
            }
        }
    }
 
    var newUrl = '/send.php?';
 
    var additionalInformation = document.querySelector('#floatingip');
    //for (i = 0; i < additionalInformation.length; i++) {
        newUrl += `${additionalInformation.id}=${additionalInformation.value}&`;
    //}
 
 
    if (errors.length > 0 || showError.innerHTML !=='') {
        showError.innerHTML += (showError.innerHTML ? '<br>' : '') + errors.join('<br>');
        return;
    } else {
       showError.style.display = "none";
    }
 
    // HIDE EVERYTHING AND SHOW LOADER
    hideEverything();
    document.getElementById('loader').style.display = "block";
 
    var result = sumResources();
    console.log(result);
 
    for (var key in data) {
        var value = data[key];
        newUrl += `${key}=${value}&`;
    }
    newUrl += `email=${email}&`;
    newUrl += `profile=${profile}&`;
    newUrl += `einfra_id=${einfraid}&`
    newUrl += `types=${result[1]}&`;
 
    window.location.href = newUrl.concat(`result=${result[0]}`);
 }

 async function sendRemove(email, profile, einfraid = '') {
    
    if(!email && !profile && !einfraid){
      var result = await fetchUserData();
      email = result.email;
      profile = result.preferredUsername;
      einfraid = result.user;
    } 
    var showError                 = document.getElementById('iseverythingok');
    var errors                    = [];
    var data                      = {};
    var description               = document.getElementById('description-of-new-project');
    var nameOfTheProject          = document.getElementById('name-of-new-project');
  
    if (nameOfTheProject.value == "" ) {
        errors.push("You must fill in a name of the project.");
    } else {
        data['Action'] = 'remove';
        data['Name_of_the_project'] = nameOfTheProject.value;
        data['Description']  = description.value;
    }

    if (errors.length > 0 || showError.innerHTML !=='') {
        showError.innerHTML += (showError.innerHTML ? '<br>' : '') + errors.join('<br>');
        return;
    } else {
       showError.style.display = "none";
    }
 
    var newUrl = '/send.php?';
 
 
    for (var key in data) {
        var value = data[key];
        newUrl += `${key}=${value}&`;
    }
    newUrl += `email=${email}&`;
    newUrl += `profile=${profile}&`;
    newUrl += `einfra_id=${einfraid}&`
 
    window.location.href = newUrl;
 }

function hideEverything() {
   document.getElementById('lines').style.display  = "none";
   document.querySelector('[id^="typeofproject-"]').style.display  = "none";
   document.getElementsByClassName('informations')[0].style.display = "none";
   document.getElementById('view-button').style.display = "none";
   document.getElementById('title-information').style.display = "none";
   document.getElementById('info-view').style.display = "none";
   document.getElementById('sum-button').style.display = "none";
}

function validatePI(input) {
    // Define a regular expression pattern for multiple Name Email pairs separated by commas
    const pattern = /^([a-zA-Z]+\s[a-zA-Z]+\s[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})(,\s?[a-zA-Z]+\s[a-zA-Z]+\s[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})*$/;
    // Check if the input matches the pattern
    if (pattern.test(input)) {
        return true; // Input matches the pattern
    } else {
        return false; // Input does not match the pattern
    }
}

function sumResources() {
   var items = document.getElementsByClassName("ciselko-v-pocitatku");
   var result = [];
   var types  = [];

   for (i = 0; i < items.length; i++) {
       if (parseInt(items[i].innerHTML) <= 0) {
           result.push(0);
           continue;
       }
       result.push(items[i].innerHTML);
       types.push(items[i].parentElement.nextSibling.nextSibling.innerText + ` - ${items[i].innerHTML}x `); //NAME OF FLAVOR
   }
   return [result, types];
}
