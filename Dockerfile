FROM php:7.4-apache

LABEL maintainer="MetaCentrum Cloud Team <cloud@ics.muni.cz>" \
      version="1.2.2" \
      description="MetaCentrum Cloud project request GUI"

ADD ./app_gui /var/www/html 
ADD ./apache2_conf /etc/apache2

RUN a2enmod ssl && a2enmod rewrite
