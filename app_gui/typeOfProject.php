<div <?php echo 'id="typeofproject-' . $formType .'"' ?>>
    <div class="row offset-top">
        <div class="col-md-12">
            <div id="new-project">
                <h4>Name of the project <?php if(in_array('name-of-new-project',$required)) echo '<span class="required">*</span>' ?></h4>
                <input id="name-of-new-project"></input>
                <?php if($formType !=='remove') { ?>
                <h4>Expiration <?php if(in_array('expiration',$required)) echo '<span class="required">*</span>' ?></h4>
                <input placeholder="dd.mm.yyyy" id="expiration"></input>
<!--            <h4 id="projectTooltip">Perun project name(?)</h4>
                <input id="claim"></input> !-->
                <h4 id="projectManagerTooltip">Project manager (project investigator - PI) (?) <?php if(in_array('name-of-project-managers',$required)) echo '<span class="required">*</span>' ?></h4>
                <input id="name-of-project-managers" data-role="tagsinput" placeholder="John Doe johndoe@gmail.com"></input>
                <h4 style='text-align:left' id="projectOrganizationTooltip">
                    Project organization (?) <?php if(in_array('project-organization',$required)) echo '<span class="required">*</span>' ?>
                </h4>    
                <select class="selectpicker d-block" id='project-organization' title="Choose one of the following..." data-live-search="true" data-width="90%" onchange="toggleProjectOrganization()">
                    <option value='custom'>custom</option>
                <?php if(isset($flavors) && array_key_exists('web::select_organizations',$flavors)) {
                    foreach($flavors['web::select_organizations'] as $key => $sOrganization ){
                        echo '<option value="' . $key . '">' . $sOrganization . '</option>';
                    }
                } ?>
                <select>
                <div id="project-organization-custom-box" style="display: none">
                    <h4>Project organization custom <?php if(in_array('project-organization-custom',$required)) echo '<span class="required">*</span>' ?></h4>
                    <input id="project-organization-custom"></input>
                </div>
                <h4 style="margin-top: 20px;">Project description <?php if(in_array('description-of-new-project',$required)) echo '<span class="required">*</span>' ?></h4>
                <textarea rows="4" id="description-of-new-project"></textarea>
                <h4 style="margin-top: 20px;" id="additionalTooltip">Additional information(?) <?php if(in_array('additional-info',$required)) echo '<span class="required">*</span>' ?></h4>
                <h5><a href="https://docs.e-infra.cz/compute/openstack/technical-reference/brno-g2-site/flavors/">List of all flavors.</a></h5>
                <textarea rows="4" id="additional-info"></textarea>
                <h4 style="margin-top: 20px;">Impact of service unavailability: 10min, 1h, 1d <?php if(in_array('impact',$required)) echo '<span class="required">*</span>' ?></h4>
                <textarea rows="4" id="impact"></textarea>
                                                        <h4 style='text-align:left'>
                                                                <label >Select number of requesting floating IPs: <?php if(in_array('floatingip',$required)) echo '<span class="required">*</span>' ?></label>
                                                                <select id='floatingip'>
                                                                    <?php if($formType === 'update') echo '<option value=0>0</option>' ?>
                                                                        <option value=1>1</option>
                                                                        <option value=2>2</option>
                                                                        <option value=3>3</option>
                                                                        <option value=4>4</option>
                                                                </select>
							</h4>
                <h4 style="margin-top: 20px;">Project by default includes 10 volumes and 1TB of storage. If you require more resources, specify them in the "additional information".</h4>

	    </div>
        <?php
        } else {
        ?>
        <h4 style="margin-top: 20px;">Additional information</h4>
        <textarea rows="4" id="description-of-new-project"></textarea>
        <?php 
        }
        ?>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    var tagsinput = document.querySelectorAll('.bootstrap-tagsinput input');
    for(i=0; i<tagsinput.length; i++){
        tagsinput[i].setAttribute('size',tagsinput[i].getAttribute('placeholder').length);
    }  
  $('#projectOrganizationTooltip').tooltip({title: "If your organization is missing, please use field 'custom' and write it into appeared input.", html: true, placement: "top"});
  $('#additionalTooltip').tooltip({title: "If you need to provide any additional information like extra flavors (eg. 2x c2.8core-30ram), storage or specific configuration please add it to this field.", html: true, placement: "top"});
  $('#projectManagerTooltip').tooltip({title: "Can be multiple projects managers, separate by ',' ", html: true, placement: "top"});
});
</script>
