<?php
  require __DIR__ . '/vendor/autoload.php';
  use Jumbojett\OpenIDConnectClient;

?>

<!DOCTYPE html>
<html lang="en">
    <?php include 'header.php';?>
	<body>
            <div class="container">
                <h1 class="text-center title">Application form for OpenStack projects</h1>
                <h2 class="text-center title">Request project removal</h2>

                     <?php 
                        require_once "api.php";
                        $formType='remove';
                        $flavors = Api::getFile();
                        $required=$flavors['web::required_inputs_common'];

                        if (array_key_exists('message', $flavors)) { 
                            echo "<h1 style='color:red' class='text-center'>Something happend</h1>";
                            echo "<p class='text-center'>". $flavors['message'] . "</p>";
                        } else {
                            include 'typeOfProject.php';
                            ?>

                            </div>
                            <h2 id="tile-information" class="offset-top text-center"></h2>
                            <div style='display:none' id="loader"></div>
                            <p id='iseverythingok' class='error'></p>
                            
                            <?php echo "<button onclick=\"sendRemove('" . $email_of_user . "', '" . $profile_of_user . "')\" id='sum-button'><b>SEND REQUEST</b></button>";
                        }
                      ?>

	    </div>
	</body>
</html>


