<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once "Spyc.php";
require_once 'SMTP.php';
require_once 'PHPMailer.php';
require_once 'Exception.php';

Class Api {
    public static $data;
    public static function getFile() {

        $result = file_get_contents('input.yaml');

        $array = Spyc::YAMLLoadString($result);
        Api::$data = $array;
        return $array;
    }

   /* In case fi you need to get file via Curl
    public static function getFile() {
        $url  = "https://gitlab.ics.muni.cz/api/v4/projects/1384/repository/files/input.yaml/raw?ref=master";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
             'PRIVATE-TOKEN:  set token',
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        $result = curl_exec($curl);

        $array = Spyc::YAMLLoadString($result);
        if (!curl_errno($curl)) {
            switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
                case 200:  # OK
                    curl_close($curl);
                    Api::$data = $array;
                    return $array;
                default:
                    curl_close($curl);
                    return ["message" => $result];
            }
        }

    }*/

    public static function send($content, $from, $profile, $types, $additional, $impact, $einfraid) {
        $mail = new PHPMailer(TRUE);
        $mail->IsSMTP();
        $mail->Host = "relay.muni.cz";
        try {
           $mail->setFrom( $from, 'Calculator');
           $mail->addAddress('cloud@metacentrum.cz', 'Cloud support');
           $mail->Subject = 'CALCULATOR: REQUEST FROM ' . $from;
           //$content = preg_replace("/\G {2}/", '', $content); //FORMAT OUTPUT (YAML FORMAT)
           $body_of_mail  = 'REQUEST FROM: ' . $profile . ' ' . $from .  "\n\n" . 
           "TYPES OF FLAVORS: " . $types . "\n" . 
           "ADDITIONAL INFORMATION: " . $additional . "\n\n" . 
           "IMPACT: " . $impact . "\n\n" . 
           "E-INFRA ID OF APPLICANT: " . $einfraid . "\n\n" .
           "INSTANCE HOST: " . $_SERVER['HTTP_HOST'] . "\n\n" .
           preg_replace('/  name:/', "- name:", $content);
           $mail->Body = $body_of_mail;
           $mail->send();
        }
        catch (Exception $e)
        {
           return $mail->ErrorInfo;
        }
        return "<p>Your request has been sent</p> <br /><a href='https://docs.e-infra.cz/compute/openstack/'>DOCUMENTATION</a>";
    }

    public static function sendRemove($projectName,$additional_info, $from, $profile, $einfraid) {
        $mail = new PHPMailer(TRUE);
        $mail->IsSMTP();
        $mail->Host = "relay.muni.cz";
        try {
           $mail->setFrom( $from, 'Calculator');
           $mail->addAddress('cloud@metacentrum.cz', 'Cloud support');
           $mail->Subject = 'CALCULATOR: REMOVAL REQUEST FROM ' . $from . ' - ' . $projectName;
    
           $body_of_mail  = "PROJECT REMOVAL REQUEST: " . "\n\n"  . 
           "PROJECT NAME: " . $projectName . "\n\n"  .
           "REQUEST FROM: " . $profile . " "  . $from .  "\n\n" . 
           "E-INFRA ID OF APPLICANT: " . $einfraid . "\n\n" .
           "ADDITIONAL INFROMATION: " . $additional_info;

           $mail->Body = $body_of_mail;
           $mail->send();
        }
        catch (Exception $e)
        {
           return $mail->ErrorInfo;
        }
        return "<p>Your request has been sent.</p>";
    }
}
