<?php
  require_once "api.php";
  include 'header.php';

  Api::getFile();
  if($_GET['Action'] === 'create' || $_GET['Action'] === 'update'){
      if (isset($_GET['result'])) {
        $user_data = explode(',',$_GET['result']);
        $data = Api::$data['cloud::profile::kolla::nova::controller::os_flavors'];
        $blacklist_flavors = Api::$data['web::blacklist_flavors'];
        $cpu  = 0;
        $ram  = 0;
        $disk = 0;
        $count_instances = 0;
        $key = 0;
    
        foreach ($data as $flav) {
         if (in_array($flav['name'], $blacklist_flavors)) {
             continue;
         }
         $cpu  += ($flav['vcpus'] * $user_data[$key]);
         $ram  += ($flav['ram'] * $user_data[$key]);
         $disk += ($flav['disk'] * $user_data[$key]);
         $count_instances = $count_instances + $user_data[$key];
         $key += 1;
        }
      }
  
      $result = array();
    
      if ($count_instances <= 5){
      $tmp_data = array("cores" => $cpu, "ram" => $ram );
      } else {
        $tmp_data = array("instances" => $count_instances, "cores" => $cpu, "ram" => $ram );
        }
    
      foreach ($_GET as $key => $value) {
         if ($key == "result" or $key == "Name_of_the_project" or $key == "Description" or $key == "Expiration" or $key == "email" or $key == "types" or $key == "Additional" or $key == "Impact" or $key == "profile" or $key == "Organization" or $key == "projectInvestigators" or $key == "einfra_id") {
            continue;
         }
         if($_GET['Action'] === 'create'){
          $tmp_data['router'] = 1;
         }
         $tmp_data[$key] = (int)$value;
      }
    
      $name_of_project_in_yaml_format = str_replace(' ', '-', $_GET["Name_of_the_project"]);
      $name_of_project_in_yaml_format = str_replace('_', '-', $name_of_project_in_yaml_format);
      $name_of_project_in_yaml_format = strtolower($name_of_project_in_yaml_format);
    
      $final_content = array("name" => $name_of_project_in_yaml_format, "domain" => "einfra_cz", "expiration" => $_GET["Expiration"], "assignments" => array(), "organization" => $_GET["Organization"], "projectInvestigators" => explode(",", $_GET["projectInvestigators"]) );
    
      if (array_key_exists("Description", $_GET)) {
          if($_GET['Action'] === 'create'){
            $final_content['description'] = $_GET["Description"].' The contact point for this project is '.$_GET["email"];
          } else {
            $final_content['description'] = $_GET["Description"];
          }
      }
    
      $final_content["quota"] = $tmp_data;
      $final_content['tags']  = array('NEED TO FILL',);
    
      $content       = array(spyc_dump(array($final_content)));
      $response = Api::send(implode("\n",$content), $_GET['email'], $_GET['profile'], $_GET['types'], $_GET['Additional'], $_GET['Impact'],$_GET['einfra_id']);
  } elseif($_GET['Action'] === 'remove') {
    $name_of_project_in_yaml_format = str_replace(' ', '-', $_GET["Name_of_the_project"]);
    $name_of_project_in_yaml_format = str_replace('_', '-', $name_of_project_in_yaml_format);
    $name_of_project_in_yaml_format = strtolower($name_of_project_in_yaml_format);
    
  
    if (array_key_exists("Description", $_GET)) {
        $additional_info= $_GET["Description"];
    } else {
      $additional_info = NULL;
    }
  
  //  echo "<pre class='text-center'>" . implode( ",\n", $content) . "</pre>";
    $response = Api::sendRemove($name_of_project_in_yaml_format,$additional_info, $_GET['email'], $_GET['profile'],$_GET['einfra_id']);
  }

  echo "<h1 class='text-center' style='margin-top: 50px;'>" . $response . "</h1>";
