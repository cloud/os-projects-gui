terraform {
  backend "swift" {
    container         = "terraform-state-os-porjects-gui"
    archive_container = "terraform-state-os-porjects-gui-archive"
  }
}

resource "openstack_compute_keypair_v2" "andy-cloud-key" {
  name       = "andy-key"
  public_key = var.public_key
}

resource "openstack_compute_instance_v2" "os-projects-gui-vm" {
  name            = "projects-request-gui"
  image_name      = "centos-8-1-1911-x86_64"
  flavor_name     = "standard.small"
  key_pair        = "${openstack_compute_keypair_v2.andy-cloud-key.id}"
  security_groups = ["${openstack_networking_secgroup_v2.os-projects-gui-secgroup.name}"]
  user_data       = "#cloud-config\nhostname: projects.cloud.muni.cz\nfqdn: projects.cloud.muni.cz"

  network {
    name = "group-project-network"
  }

}

resource "openstack_networking_secgroup_v2" "os-projects-gui-secgroup" {
  name        = "os-projects-gui-secgroup"
  description = "Security group for OpenStack projects request GUI site"
}

resource "openstack_networking_secgroup_rule_v2" "https" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.os-projects-gui-secgroup.id}"
}

resource "openstack_networking_secgroup_rule_v2" "http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.os-projects-gui-secgroup.id}"
}

resource "openstack_networking_secgroup_rule_v2" "ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.os-projects-gui-secgroup.id}"
}

resource "openstack_networking_floatingip_v2" "os-projects-gui-fip" {
  pool = "public-cesnet-78-128-251-GROUP"
}

resource "openstack_compute_floatingip_associate_v2" "fip_1" {
  floating_ip = var.floating_ip 
  instance_id = "${openstack_compute_instance_v2.os-projects-gui-vm.id}"
}

