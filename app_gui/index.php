<?php
  require __DIR__ . '/vendor/autoload.php';
  use Jumbojett\OpenIDConnectClient;

  if(getenv('oidc_enable') === 'true'){  
    $credentials  = "oidc_credentials.txt";
    if (file_exists($credentials)) {
      $lines = file($credentials, FILE_IGNORE_NEW_LINES);
    } else {
      $lines = [
        '',
        getenv('oidc_client_id'),
        getenv('oidc_client_secret')
      ];
    }
    
    $oidc = new OpenIDConnectClient(
      'https://login.e-infra.cz/oidc/',
      $lines[1],
      $lines[2]
    );
    $oidc->setRedirectURL(getenv('oidc_redirect_url'),);
    $oidc->addScope('email');
    $oidc->addScope('profile');
    $oidc->addScope('openid');
    $oidc->authenticate();
    $email_of_user = $oidc->requestUserInfo('email');
    $profile_of_user = $oidc->requestUserInfo('name');
  }
?>

<!DOCTYPE html>
<html lang="en">
    <?php include 'header.php';?>
	<body>
            <div class="container">
                <h1 class="text-center title">Choose action</h1>
                
                <a class="home-tile" href="create.php">
                    <div class="tile-title">
                        Request new project
                    </div>
                    <div class="tile-description">
                        To request new group project please use this form.
                    </div> 
                </a>
                <a class="home-tile" href="update.php">
                    <div class="tile-title">
                        Request project update
                    </div>
                    <div class="tile-description">
                    To request quota increase, access to particular flavor or extend project lifetime (project name and new date are the only neccesary fields), please use this form.
                    </div> 
                </a>
                <a class="home-tile" href="remove.php">
                    <div class="tile-title">
                        Request project removal
                    </div>
                    <div class="tile-description">
                    To request group project removal, please use this form.
                    </div> 
                </a>
	        </div>
	</body>
</html>


