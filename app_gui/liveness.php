<?php
// Serves liveness and readiness probe
// Set the content type header to JSON
header('Content-Type: application/json');

// Create the JSON object
$response = array(
    'live' => 'ok'
);

// Encode the array to JSON format
$json_response = json_encode($response);

// Output the JSON response
echo $json_response;
?>

